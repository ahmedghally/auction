<?php
use App\Product;

Auth::routes();
Route::group(['middleware' => ['web','auth']], function(){
    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/home', function() {
        if (Auth::user()->admin == 0) {
            return view('user.userhome');
        } else {
            $users['users'] = \App\User::all();
            return view('admin.adminhome', $users);
        }
    });
});
Route::get('/product',"productcontroller@viewproduct");
Route::get('/searchproduct',"productcontroller@searchproduct");
Route::post('searchproduct',"productcontroller@searchproduct");

Route::get('addproduct',"productcontroller@addproduct");
Route::post('addproduct',"productcontroller@addproduct");
Route::get('addproduct/{id}',"productcontroller@deleteproduct");
Route::get('edit/{id}',"productcontroller@editproduct");
Route::post('edit/{id}',"productcontroller@editproduct");


Route::get('/user',"usercontroller@viewuser");
Route::get('/searchuser',"usercontroller@searchuser");
Route::post('searchuser',"usercontroller@searchuser");
Route::get('adduser',"usercontroller@adduser");
Route::post('adduser',"usercontroller@adduser");
Route::get('adduser/{id}',"usercontroller@deleteuser");

Route::get('/create',"seassioncontroller@create");
Route::post('create',"seassioncontroller@create");
Route::get('/seassion',"seassioncontroller@viewseassion");
Route::get('/startseassion',"seassioncontroller@viewstartseassion");

Route::get('startseassion/{id}',"seassioncontroller@startseassion");
Route::post('startseassion/{id}',"seassioncontroller@startseassion");

Route::get('/adminhome',"admincontroller@viewhome");
Route::get('/v',"admincontroller@v");

Route::get('/userhome',"usercontroller@viewhome");
Route::get('/userproduct',"usercontroller@viewproduct");

