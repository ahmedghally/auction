<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Array_;
use App\Product;

class productcontroller extends Controller
{
    //
    public function viewproduct()
    {
        $product=Product::all();
        $arr=Array('product'=>$product);
        return view('product.view',$arr);
    }
    public function searchproduct(Request $request)
    {
        if($request->isMethod('post')){
        $id=$request->input('search');

        $product=Product::find($id);
        $arr=Array('product'=>$product);
        return view('product.search',$arr);
    }
    else{
        $product=Product::find(1);
        $arr=Array('product'=>$product);
        return view('product.search',$arr);
    }


    }

    public function addproduct(Request $request)
    {
        if($request->isMethod('post')){
            $newproduct=new Product();
            $newproduct->name=$request->input('name');
            $newproduct->type=$request->input('type');
             $newproduct->price=$request->input('price');
             $newproduct->save();

        }
    	return view('product.add');
    }
    public function editproduct(Request $request,$id)
    {
        if($request->isMethod('post')){
            $newproduct=Product::find($id);
            $newproduct->name=$request->input('name');
            $newproduct->type=$request->input('type');
             $newproduct->price=$request->input('price');
             $newproduct->save();
            return redirect('product');
        }
        else{
        $product=Product::find($id);
        $arr=Array('product'=>$product);


        return view('product.edit',$arr);
        }
    }
    public function deleteproduct($id)
    {
    $product=Product::find($id);
    $product->delete();
    return redirect("product");
    }

}
