<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Array_;
use App\User;
use App\Product;

class usercontroller extends Controller
{
       public function  viewhome(){
        return view('user.home');
    }
	    public function viewuser()
    {
        $user=User::all();
        $arr=Array('user'=>$user);
        return view('auth.view',$arr);
    }
    public function searchuser(Request $request)
   {
        if($request->isMethod('post')){
        $id=$request->input('search');

        $user=User::find($id);
        $arr=Array('user'=>$user);
        return view('auth.search',$arr);
    }
    else{
        $user=User::find(1);
        $arr=Array('user'=>$user);
        return view('auth.search',$arr);
    }


    }
    public function adduser(Request $request)
    {
        if($request->isMethod('post')){
            $newuser=new User();
            $newuser->name=$request->input('name');
            $newuser->email=$request->input('email');
             $newuser->password=$request->input('password');
             $newuser->save();

        }
    	return view('auth.adduser');
    }
    public function deleteuser($id)
    {
    $user=User::find($id);
    $user->delete();
    return redirect("user");
    }
        public function viewproduct()
    {
        $product=Product::all();
        $arr=Array('product'=>$product);
        return view('user.userproduct',$arr);
    }
    //
}
